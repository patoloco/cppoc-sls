/* eslint-disable no-param-reassign */

const _ = require('lodash');

// let logLevel = 'INFO';
let logLevel = 'DEBUG';
if (process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'debug') {
  logLevel = 'DEBUG';
}

const log4js = require('log4js');

const defaultLogger = log4js.getLogger('default');
defaultLogger.level = logLevel;

function camelAllKeys(object) {
  Object.keys(object).forEach((key) => {
    const newKey = _.camelCase(key);
    object[newKey] = object[key];
    if (newKey !== key) {
      delete object[key];
    }
  });
}

function snakeAllKeys(object) {
  Object.keys(object).forEach((key) => {
    const newKey = _.snakeCase(key);
    object[newKey] = object[key];
    if (newKey !== key) {
      delete object[key];
    }
  });
}


module.exports = {
  camelAllKeys,
  log: defaultLogger,
  snakeAllKeys,
};
