/* eslint-disable */

const _ = require('lodash');
const chai = require('chai');
const Lab = require('lab');

const lab = exports.lab = Lab.script();

const { describe, it } = lab;
const { expect } = chai;

const { camelAllKeys, snakeAllKeys } = require('./index');

const snakeFarm = {
  id: 12345,
  grower_id: 'abc999def',
  name: 'good old farm',
  address: '55 Nowhere Lane',
  created_at: '2018-12-06T23:13:40.907Z',
  updated_at: '2018-12-06T23:13:40.907Z'
};

const camelFarm = {
  id: 12345,
  growerId: 'abc999def',
  name: 'good old farm',
  address: '55 Nowhere Lane',
  createdAt: '2018-12-06T23:13:40.907Z',
  updatedAt: '2018-12-06T23:13:40.907Z'
};

describe('Utils', () => {
  describe('camelAllKeys()', () => {
    it('converts a snake-cased object\'s keys to camelCase', (done) => {
      const test = _.cloneDeep(snakeFarm);
      const numKeys = Object.keys(test).length;
      camelAllKeys(test);

      expect(test).to.have.all.keys('id', 'growerId', 'name', 'address', 'createdAt', 'updatedAt');
      expect(test).to.not.have.property('grower_id');
      expect(test).to.not.have.property('created_at');
      expect(test).to.not.have.property('updated_at');

      const numKeysAfter = Object.keys(test).length;
      expect(numKeys).to.equal(numKeysAfter);

      done();
    });

    it('does not affect a camelCased object\'s keys', (done) => {
      const test = _.cloneDeep(camelFarm);
      const numKeys = Object.keys(test).length;
      camelAllKeys(test);

      expect(test).to.have.all.keys('id', 'growerId', 'name', 'address', 'createdAt', 'updatedAt');
      const numKeysAfter = Object.keys(test).length;
      expect(numKeys).to.equal(numKeysAfter);

      done();
    });
  });

  describe('snakeAllKeys()', () => {
    it('converts a camelCased object\'s keys to snake case', (done) => {
      const test = _.cloneDeep(camelFarm);
      const numKeys = Object.keys(test).length;
      snakeAllKeys(test);

      expect(test).to.have.all.keys('id', 'grower_id', 'name', 'address', 'created_at', 'updated_at');
      expect(test).to.not.have.property('growerId');
      expect(test).to.not.have.property('createdAt');
      expect(test).to.not.have.property('updatedAt');

      const numKeysAfter = Object.keys(test).length;
      expect(numKeys).to.equal(numKeysAfter);

      done();
    });

    it('does not affect a snake cased object\'s keys', (done) => {
      const test = _.cloneDeep(snakeFarm);
      const numKeys = Object.keys(test).length;
      snakeAllKeys(test);

      expect(test).to.have.all.keys('id', 'grower_id', 'name', 'address', 'created_at', 'updated_at');
      const numKeysAfter = Object.keys(test).length;
      expect(numKeys).to.equal(numKeysAfter);

      done();
    });
  });
});
