class h {
  constructor() {
    this.res = {};
  }

  code(code) {
    this.res.statusCode = code;
    return this.res;
  }

  type(type) {
    this.res.headers = {
      'content-type': type,
    };
    return {
      code: this.code.bind(this),
    };
  }

  response(data) {
    this.res.source = data;
    return {
      type: this.type.bind(this),
    };
  }

  okay(data) {
    this
      .response(data)
      .type('application/json')
      .code(200);

    return this.res;
  }

  created(data) {
    this
      .response(data)
      .type('application/json')
      .code(201);

    return this.res;
  }

  accepted(data) {
    this
      .response(data)
      .type('application/json')
      .code(202);

    return this.res;
  }

  noContent(data) {
    this
      .response(data)
      .type('application/json')
      .code(204);

    return this.res;
  }
}

module.exports = h;
