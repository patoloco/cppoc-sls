module.exports = {
  info: {
    title: 'Crop Planning API Documentation',
    version: '1.0.0',
  },
  cors: true,
  documentationPath: '/api_docs',
  grouping: 'tags',
  securityDefinitions: {
    jwt: {
      type: 'apiKey',
      name: 'Authorization',
      in: 'header',
    },
  },
  sortEndpoints: 'path',
  jsonEditor: true,
  tags: [
    {
      name: 'farms',
      description: 'Farms endpoint',
    },
  ],
};
