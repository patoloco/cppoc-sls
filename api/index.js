const Products = require('./products');
const pkg = require('./package.json');

const register = async (server, options) => {
  server.register(Products, options);
};

const plugin = {
  pkg,
  register,
};

module.exports = plugin;
