const productService = require('./services');

const dependencies = {
  productService,
};

async function getProductHandler(request, h) {
  try {
    const products = await productService.getProducts(request.query);

    return h.okay(products);
  } catch (err) {
    return h.okay('failed');
  }
}

module.exports = {
  getProductHandler,
  dependencies,
};
