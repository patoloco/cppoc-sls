const Joi = require('joi');

// --------------------------------------------------
//    Schema - Output Validations
// --------------------------------------------------

const ProductJSON = Joi.object().keys({
  id: Joi.any().description('unique productId'),
  name: Joi.string().required().description('product name'),
  price: Joi.number().required().description('product price'),
});

const ListProductsOutputPayload = Joi.array().items(ProductJSON);


// --------------------------------------------------
//    Config - Output Validations
// --------------------------------------------------

const ListProductsOutputValidationsConfig = {
  status: {
    200: ListProductsOutputPayload,
  },
};

module.exports = {
  ProductJSON,
  ListProductsOutputValidationsConfig,
};
