const Joi = require('joi');
const { validateOptions } = require('../../../../utils/validations');

// --------------------------------------------------
//    Config - Input Validations
// --------------------------------------------------

const ProductsQuery = {
  query: {
    minPrice: Joi.number().description('filter by min price'),
    name: Joi.string().description('filter by name'),
  },
  options: validateOptions.options,
  failAction: validateOptions.failAction,
};

module.exports = {
  ProductsQuery,
};
