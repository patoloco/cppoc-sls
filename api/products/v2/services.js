const _ = require('lodash');

const products = [
  {
    name: 'Miracle-gro',
    price: 100,
  },
  {
    name: 'Fox Farm',
    price: 50,
  },
  {
    name: 'Eraser',
    price: 75,
  },
];

async function getProducts(query) {
  const filteredByPrice = _.filter(products, product => product.price >= (query.minPrice || 0));
  const filteredByName = _.filter(filteredByPrice, product => _.includes(product.name, query.name || ''));

  return filteredByName;
}

module.exports = {
  getProducts,
};
