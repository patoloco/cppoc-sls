const inputValidations = require('./validations/input');
const outputValidations = require('./validations/output');
const handlers = require('./handlers');

const getProducts = {
  method: 'GET',
  path: '/v2/products',
  config: {
    description: 'Get a list of farms or query by name or growerId',
    notes: 'Return a list of farms',
    tags: ['api', 'farms'],
    response: outputValidations.ListProductsOutputValidationsConfig,
    validate: inputValidations.ProductsQuery,
  },
  handler: handlers.getProductHandler,
};

module.exports = [
  getProducts,
];
