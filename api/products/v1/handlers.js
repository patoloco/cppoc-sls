const dynamoProductService = require('./dynamoProductService');

const { log } = require('../../../utils');

const dependencies = {
  dynamoProductService,
};

async function getProductHandler(request, h) {
  try {
    if (!request.query || !request.query.productId && !request.query.name) {
      return h.okay({ errorMsg: 'bad request' });
    }

    if (request.query.productId) {
      const product = await dependencies.dynamoProductService
        .getProductByProductId(request.query.productId);

      if (product && product.productId) {
        return h.okay([product]);
      }
      return h.okay([]);
    } else if (request.query.name) {
      const products = await dependencies.dynamoProductService
        .getProducts({ name: request.query.name, queryType: 'match' });

      log.debug('products in handler:', products);
      if (products && products.length > 0) {
        return h.okay(products);
      }
      return h.okay([]);
    }

    return h.okay({ errorMsg: 'bad request' });
  } catch (err) {
    log.error('err:', err);
    return h.okay('failed');
  }
}

async function createProductHandler(request, h) {
  try {
    const product = request.payload;
    const result = await dependencies.dynamoProductService.createProduct(product);
    return h.created(result);
  } catch (err) {
    log.error('err:', err);
    return h.okay('failed');
  }
}

module.exports = {
  createProductHandler,
  getProductHandler,
  dependencies,
};
