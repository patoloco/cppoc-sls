const Joi = require('joi');

// --------------------------------------------------
//    Schema - Output Validations
// --------------------------------------------------

const ProductJSON = Joi.object().keys({
  productId: Joi.any().description('unique productId'),
  name: Joi.string().required().description('product name'),
  price: Joi.number().required().description('product price'),
});

const ListProductsOutputPayload = Joi.array().items(ProductJSON).optional();

// --------------------------------------------------
//    Config - Output Validations
// --------------------------------------------------

const ListProductsOutputValidationsConfig = {
  status: {
    200: ListProductsOutputPayload,
  },
};

const SingleProductOutputValidationsConfig = {
  status: {
    200: ProductJSON,
  },
};

const CreateProductOutputValidationsConfig = {
  status: {
    201: ProductJSON,
  },
};

module.exports = {
  CreateProductOutputValidationsConfig,
  ProductJSON,
  ListProductsOutputValidationsConfig,
  SingleProductOutputValidationsConfig,
};
