const Joi = require('joi');
const { validateOptions } = require('../../../../utils/validations');

// --------------------------------------------------
//    Config - Input Validations
// --------------------------------------------------

const ProductsQuery = {
  query: {
    name: Joi.string().description('search by name'),
    productId: Joi.string().description('search by productId'),
  },
  // headers: HeadersPayLoad,
  options: validateOptions.options,
  failAction: validateOptions.failAction,
};

const CreateProductBody = {
  payload: {
    name: Joi.string().required().description('product name'),
    price: Joi.number().required().description('proudct price'),
  },
  options: validateOptions.options,
  failAction: validateOptions.failAction,
};

module.exports = {
  CreateProductBody,
  ProductsQuery,
};
