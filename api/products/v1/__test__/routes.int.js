/* eslint-disable consistent-return */

const { expect, assert } = require('chai');
const http = require('http');
const Lab = require('lab');

const LabbableServer = require('../../../../');

const lab = exports.lab = Lab.script();

const {
  before, describe, it, after,
} = lab;

describe('Products::', () => {
  let server;

  before((done) => {
    LabbableServer.ready((err, srv) => {
      if (err) {
        return done(err);
      }

      server = srv;

      done();
    });
  });

  describe('v1::', () => {
    it('Should make request to get v1 products', (done) => {
      const injectOptions = {
        method: 'GET',
        url: '/api/products?maxPrice=80&name=Fox',
        headers: {
          'api-version': 1,
        },
      };
      server.inject(injectOptions).then((res) => {
        expect(res.statusCode).to.be.equal(200);
        const products = JSON.parse(res.payload);

        expect(products).to.be.an('array').with.length.greaterThan(0);
        expect(products).to.deep.equal([{
          name: 'Fox Farm',
          price: 50,
        }]);
        done();
      }).catch(() => {
        expect.fail();
        return done();
      });
    });
    it('Unspecified version should fail because of v2', (done) => {
      const injectOptions = {
        method: 'GET',
        url: '/api/products?maxPrice=40&name=Fox',
      };
      server.inject(injectOptions).then((res) => {
        expect(res.statusCode).to.be.equal(500);
        done();
      }).catch(() => {
        expect.fail();
        return done();
      });
    });
  });
});
