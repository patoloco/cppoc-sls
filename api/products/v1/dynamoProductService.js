/* eslint-disable no-param-reassign */

const uuid = require('uuid/v1');

const AWS = require('aws-sdk');

AWS.config.apiVersions = {
  dynamodb: '2012-08-10',
};
const dynamo = new AWS.DynamoDB.DocumentClient();

const { log } = require('../../../utils');
const messages = require('../../../shared/errors/messages');

const TableName = process.env.productsTable;
const IndexName = process.env.productsTableNameIndex;

function createProduct(Item) {
  return new Promise((resolve, reject) => {
    // todo: change to joi validation or such
    if (!Item.name || !Item.price) {
      reject(new Error(messages.dynamo.generic.missingRequired));
    } else {
      Item.productId = uuid();

      const params = {
        TableName,
        Item,
      };

      dynamo.put(params, (err, data) => {
        if (err) {
          log.error('err:', err);
          reject(err);
        } else {
          log.debug('data:', data);
          resolve(Item);
        }
      });
    }
  });
}

function getProducts(options) {
  return new Promise((resolve, reject) => {
    if (!options) {
      reject(new Error(messages.dynamo.products.get.badOptions));
    } else if (!options.name || !options.queryType || !['match'].includes(options.queryType)) {
      reject(new Error(messages.dynamo.products.get.badOptions));
    } else {
      const ExpressionAttributeNames = { '#name': 'name' };
      const ExpressionAttributeValues = { ':name': options.name };
      const KeyConditionExpression = '#name = :name';

      const params = {
        ExpressionAttributeNames,
        ExpressionAttributeValues,
        IndexName,
        Limit: 50,
        KeyConditionExpression,
        TableName,
      };

      dynamo.query(params, (err, data) => {
        if (err) {
          log.error('err:', err);
          reject(err);
        } else {
          log.debug('data:', data);
          if (!data.Items || data.Items.length === 0) {
            resolve([]);
          } else {
            // we have up to 50 productIds here
            const Keys = [];
            data.Items.forEach((item) => {
              Keys.push({
                productId: item.productId,
              });
            });

            const batchParams = {
              RequestItems: {
                [TableName]: { Keys },
              },
            };

            dynamo.batchGet(batchParams, (batchErr, batchData) => {
              if (batchErr) {
                log.error('batchErr:', batchErr);
                reject(batchErr);
              } else {
                const items = batchData.Responses[TableName];
                log.debug('batchData items:', items);
                resolve(items);
              }
            });
          }
        }
      });
    }
  });
}

function getProductByProductId(productId) {
  return new Promise((resolve, reject) => {
    if (!productId) {
      reject(new Error(messages.dynamo.products.get.badOptions));
    } else {
      const params = {
        TableName,
        Key: {
          productId,
        },
      };

      dynamo.get(params, (err, data) => {
        if (err) {
          log.error('err:', err);
          reject(err);
        } else {
          log.debug('data:', data);
          resolve(data.Item || {});
        }
      });
    }
  });
}

module.exports = {
  createProduct,
  getProductByProductId,
  getProducts,
};
