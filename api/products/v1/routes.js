const inputValidations = require('./validations/input');
const outputValidations = require('./validations/output');
const handlers = require('./handlers');

const getProducts = {
  method: 'GET',
  path: '/v1/products',
  config: {
    description: 'Get a list of farms or query by name or growerId',
    notes: 'Return a list of farms',
    tags: ['api', 'products'],
    response: outputValidations.ListProductsOutputValidationsConfig,
    validate: inputValidations.ProductsQuery,
  },
  handler: handlers.getProductHandler,
};

const createProduct = {
  method: 'POST',
  path: '/v1/products',
  config: {
    description: 'Create a product',
    notes: 'Create a product',
    tags: ['api', 'products'],
    response: outputValidations.CreateProductOutputValidationsConfig,
    validate: inputValidations.CreateProductBody,
  },
  handler: handlers.createProductHandler,
};

module.exports = [
  createProduct,
  getProducts,
];
