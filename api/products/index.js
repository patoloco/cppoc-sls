const v1 = require('./v1/routes');
const v2 = require('./v2/routes');

const pkg = require('./package.json');

const register = async (server, options) => {
  server.route(v1);
  server.route(v2);
};

const plugin = { pkg, register };

module.exports = plugin;
