module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "no-multi-assign": 1,
        "no-plusplus": [2, { allowForLoopAfterthoughts: true }],
        "no-return-await": 0,
        "no-shadow": 1,
        "no-unused-vars": 1,
        "prefer-destructuring": 1,
        "semi": [2, "always"]
    }
};