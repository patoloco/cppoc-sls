const Hapi = require('hapi');

const apiVersion = require('hapi-api-version');

const Inert = require('inert');
const Vision = require('vision');

const HapiSwagger = require('hapi-swagger');
const lambdaHttp = require('lambda-http');
const swaggerOptions = require('./swaggerOptions');

const api = require('./api');

const responses = require('./shared/responses');

const { log } = require('./utils');

let server;

async function startServer(port, callback) {
  server = new Hapi.Server({ port });

  await server.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions,
    },
    {
      plugin: apiVersion,
      options: {
        validVersions: [1, 2],
        defaultVersion: 1,
        vendorName: 'vendor',
        basePath: '/api/',
      },
    },
    {
      plugin: api,
      options: {
        routes: {
          prefix: '/api',
        },
      },
    },
  ]);

  server.decorate('toolkit', 'okay', responses.okay);
  server.decorate('toolkit', 'created', responses.created);
  server.decorate('toolkit', 'accepted', responses.accepted);
  server.decorate('toolkit', 'noContent', responses.noContent);

  try {
    await server.start();
    if (callback) {
      callback(null);
    }
  } catch (e) {
    log.error('e:', e);
    if (callback) {
      callback(e);
    }
  }
}

exports.handler = lambdaHttp.newHandler(async (socketPath, callback) => {
  startServer(socketPath, callback);
});

if (process.env.RUN_LOCAL === 'true') {
  startServer(1337);
  log.info('server started');
}
