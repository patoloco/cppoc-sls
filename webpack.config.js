const path = require('path');

const slsw = require('serverless-webpack');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');

const mode = process.env.NODE_ENV !== 'production' ? 'development' : 'production';

module.exports = {
  entry: slsw.lib.entries,
  externals: [nodeExternals()],
  mode,
  output: {
    path: path.join(__dirname, 'out'),
    filename: 'index.js',
    libraryTarget: 'commonjs',
  },
  target: 'node',
};
