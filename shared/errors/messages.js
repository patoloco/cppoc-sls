module.exports = {
  defaultError: 'something went wrong',
  dynamo: {
    generic: {
      missingRequired: 'missing required field',
    },
    products: {
      get: {
        badOptions: 'bad getProduct options',
      },
    },
  },
};
