// eslint-disable-next-line
const okay = function (data) {
  return this
    .response(data)
    .type('application/json')
    .code(200);
};

// eslint-disable-next-line
const created = function (data) {
  return this
    .response(data)
    .type('application/json')
    .code(201);
};

// eslint-disable-next-line
const accepted = function (data) {
  return this
    .response(data)
    .type('application/json')
    .code(202);
};

// eslint-disable-next-line
const noContent = function (data) {
  return this
    .response(data)
    .type('application/json')
    .code(204);
};

module.exports = {
  okay,
  created,
  accepted,
  noContent,
};
